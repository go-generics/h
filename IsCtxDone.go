package h

import "context"

// IsCtxDone выполняет проверку завершенности переданного контекста
// и возвращает флаг, показывающий это. Если возвращается True -
// контекст завершен.
func IsCtxDone(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
	}

	return false
}
