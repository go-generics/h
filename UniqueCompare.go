package h

// UniqueCompare возвращает уникальные элементы из переданного массива.
// В результат возвращаются все элементы в единичном экземпляре.
// Функция работает со структурами, реализующими интерфейс Comparable
func UniqueCompare[T Comparable](items []T) []T {
	result := make([]T, 0, len(items))
	for _, item := range items {
		if !InArrayCompare(item, result) {
			result = append(result, item)
		}
	}

	return result
}
