package h

import "testing"

func TestIsNil(t *testing.T) {
	var ptr *struct{}
	type args struct {
		value any
	}
	type testCase struct {
		name string
		args args
		want bool
	}
	tests := []testCase{
		{
			name: "Тестирование на скалярных типах",
			args: args{
				value: "test",
			},
			want: false,
		},
		{
			name: "Тестирование на структурах",
			args: args{
				value: struct{}{},
			},
			want: false,
		},
		{
			name: "Тестирование на указателях",
			args: args{
				value: ptr,
			},
			want: true,
		},
		{
			name: "Тестирование на указателях",
			args: args{
				value: &struct{}{},
			},
			want: false,
		},
		{
			name: "Тестирование на nil",
			args: args{
				value: nil,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsNil(tt.args.value); got != tt.want {
				t.Errorf("IsNil() = %v, want %v", got, tt.want)
			}
		})
	}
}
