package h

// IntersectCompare вычисляет пересечение массивов Comparable.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// найдены во всех остальных переданных массивах.
func IntersectCompare[T Comparable](array []T, arraysToIntersect ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isFound := true

		for _, arrayToDiff := range arraysToIntersect {
			if !InArrayCompare(item, arrayToDiff) {
				isFound = false

				break
			}
		}

		if isFound {
			result = append(result, item)
		}
	}

	return result
}
