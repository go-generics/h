package h

import (
	"reflect"
	"testing"
)

func TestInterfaceToType(t *testing.T) {
	type args struct {
		items []interface{}
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование приведения типов",
			args: args{
				items: []interface{}{
					`1`,
					`2`,
					`3`,
				},
			},
			want: []string{`1`, `2`, `3`},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InterfaceToType[string](tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InterfaceToType() = %v, want %v", got, tt.want)
			}
		})
	}
}
