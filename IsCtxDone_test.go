package h

import (
	"context"
	"testing"
	"time"
)

func TestIsCtxDone(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Millisecond)
	defer cancel()

	time.Sleep(2 * time.Millisecond)

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование на живом контексте",
			args: args{
				ctx: context.Background(),
			},
			want: false,
		},
		{
			name: "Тестирование на живом контексте",
			args: args{
				ctx: ctx,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsCtxDone(tt.args.ctx); got != tt.want {
				t.Errorf("IsCtxDone() = %v, want %v", got, tt.want)
			}
		})
	}
}
