package h

// InArrayCompare проверяет наличие переданного элемента в переданном
// массиве.
//
// Для использования допускаются только типы, реализующие специальный
// тип - Comparable.
//
// В случае, если элемент есть в переданном массиве, возвращается TRUE,
// иначе FALSE.
func InArrayCompare[T Comparable](item T, array []T) bool {
	for _, exists := range array {
		if item.CKey() == exists.CKey() {
			return true
		}
	}

	return false
}
