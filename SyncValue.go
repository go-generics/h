package h

import "sync"

// SyncValueStruct описывает структуру, содержащую предустановленное значение типа T,
// которое можно менять при помощи встроенного в объект мьютекса не опасаясь
// ошибки конкурентного доступа.
type SyncValueStruct[T any] struct {
	value T
	mx    *sync.RWMutex
}

// Get возвращает текущее значение объекта
func (s *SyncValueStruct[T]) Get() T {
	s.mx.RLock()
	defer s.mx.RUnlock()

	return s.value
}

// Set выполняет установку нового значения для объекта со значением
func (s *SyncValueStruct[T]) Set(value T) {
	s.mx.Lock()
	defer s.mx.Unlock()

	s.value = value
}

// SyncValue генерирует объект переданного типа, содержащий структуру, содержащую
// предустановленное значение типа T, которое можно менять при помощи встроенного
// в объект мьютекса не опасаясь ошибки конкурентного доступа.
func SyncValue[T any](initial T) *SyncValueStruct[T] {
	return &SyncValueStruct[T]{
		value: initial,
		mx:    &sync.RWMutex{},
	}
}
