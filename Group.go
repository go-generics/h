package h

// Group выполняет группировку элементов переданных массивов по какому либо
// признаку.
//
// В качестве источника для получения признака группировки в функцию передается
// callback первым параметром
func Group[T any](groupCompare func(a, b T) bool, items ...[]T) [][]T {
	allItems := Flat(items)
	result := make([][]T, 0, len(allItems))

	groupedKeys := make([]int, 0, len(allItems))

	for i, item := range allItems {
		if InArray(i, groupedKeys) {
			continue
		}

		itemGroup := make([]T, 0, len(allItems))

		for j, secondaryItem := range allItems {
			if groupCompare(item, secondaryItem) {
				groupedKeys = append(groupedKeys, j)
				itemGroup = append(itemGroup, secondaryItem)
			}
		}

		result = append(result, itemGroup)
	}

	return result
}
