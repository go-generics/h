package h

import "time"

// EorSOne выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func EorSOne[Arg any](
	callback func(Arg) error,
) func(timeout time.Duration) func(arg Arg) error {
	return func(timeout time.Duration) func(arg Arg) error {
		return func(arg Arg) error {
			_, err := WaitOrSkip(timeout, func() (any, error) {
				return nil, callback(arg)
			})

			return err
		}
	}
}
