package h

import (
	"reflect"
	"testing"
)

func TestParseType(t *testing.T) {
	type TestStruct struct {
		Test string `mapstructure:"test"`
	}

	type args struct {
		data interface{}
	}
	type testCase struct {
		name    string
		args    args
		want    *TestStruct
		wantErr bool
	}
	tests := []testCase{
		{
			name: "Тестирование определения структуры",
			args: args{
				data: struct{}{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование определения структуры",
			args: args{
				data: TestStruct{
					Test: `test`,
				},
			},
			want: &TestStruct{
				Test: `test`,
			},
			wantErr: false,
		},
		{
			name: "Тестирование определения структуры",
			args: args{
				data: map[string]any{
					`test`: `test`,
				},
			},
			want: &TestStruct{
				Test: `test`,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseType[TestStruct](tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseType() got = %v, want %v", got, tt.want)
			}
		})
	}
}
