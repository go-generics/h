package h

import "testing"

func TestIsEmpty(t *testing.T) {
	var ptr *struct{}

	testChan := make(chan string, 2)
	testChan <- `1`

	type args struct {
		value any
	}
	type testCase struct {
		name string
		args args
		want bool
	}
	tests := []testCase{
		{
			name: "Тестирование скалярного значения",
			args: args{
				value: 123,
			},
			want: false,
		},
		{
			name: "Тестирование скалярного значения",
			args: args{
				value: ``,
			},
			want: true,
		},
		{
			name: "Тестирование массива",
			args: args{
				value: []string{},
			},
			want: true,
		},
		{
			name: "Тестирование массива",
			args: args{
				value: [5]string{`1`, `2`, `3`, `4`, `5`},
			},
			want: false,
		},
		{
			name: "Тестирование map",
			args: args{
				value: map[int]string{1: `test`},
			},
			want: false,
		},
		{
			name: "Тестирование канала",
			args: args{
				value: make(chan string),
			},
			want: true,
		},
		{
			name: "Тестирование канала",
			args: args{
				value: testChan,
			},
			want: false,
		},
		{
			name: "Тестирование указателя",
			args: args{
				value: &struct{}{},
			},
			want: true,
		},
		{
			name: "Тестирование указателя",
			args: args{
				value: struct {
					Test string
				}{`123`},
			},
			want: false,
		},
		{
			name: "Тестирование указателя",
			args: args{
				value: ptr,
			},
			want: true,
		},
		{
			name: "Тестирование структуры",
			args: args{
				value: struct {
					Test string
				}{`123`},
			},
			want: false,
		},
		{
			name: "Тестирование структуры",
			args: args{
				value: struct {
					Test string
				}{},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsEmpty(tt.args.value); got != tt.want {
				t.Errorf("IsEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}
