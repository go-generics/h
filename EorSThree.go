package h

import "time"

// EorSThree выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func EorSThree[Arg any, Arg2 any, Arg3 any](
	callback func(Arg, Arg2, Arg3) error,
) func(timeout time.Duration) func(Arg, Arg2, Arg3) error {
	return func(timeout time.Duration) func(Arg, Arg2, Arg3) error {
		return func(arg Arg, arg2 Arg2, arg3 Arg3) error {
			_, err := WaitOrSkip(timeout, func() (any, error) {
				return nil, callback(arg, arg2, arg3)
			})

			return err
		}
	}
}
