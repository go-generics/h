package h

import (
	"reflect"
	"testing"
)

func TestIntersectFn(t *testing.T) {
	type args struct {
		compareFn         func(a, b string) bool
		array             []string
		arraysToIntersect [][]string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование получения пересечения массивов",
			args: args{
				compareFn: func(a, b string) bool {
					return a == b
				},
				array: []string{"1", "2", "3"},
				arraysToIntersect: [][]string{
					{"2", "3", "4"},
					{"2", "1", "22"},
				},
			},
			want: []string{"2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IntersectFn(tt.args.compareFn, tt.args.array, tt.args.arraysToIntersect...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("IntersectFn() = %v, want %v", got, tt.want)
			}
		})
	}
}
