package h

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"reflect"
)

// ParseType выполняет парсинг не типизированного значения
// если оно соответствует переданному типу, возвращает его
// Если это мапа, то пытается смапать результат в требуемую
// сущность.
func ParseType[T any](data any) (*T, error) {
	var entity T
	switch typed := data.(type) {
	case T:
		entity = typed
	case *T:
		entity = *typed
	case map[string]any:
		if _, ok := ToInterface(entity).(map[string]any); ok {
			result := ToInterface(typed).(T)

			return &result, nil
		}

		if reflect.ValueOf(entity).Kind() != reflect.Struct {
			return nil, errors.New(`[ParseType] only structures allowed to map values`)
		}

		err := mapstructure.Decode(typed, &entity)
		if nil != err {
			return nil, errors.Wrap(err, `[ParseType] failed to map data to destination`)
		}
	default:
		return nil, errors.New(`[ParseType] invalid entity type`)
	}

	return &entity, nil
}
