package h

import (
	"reflect"
	"testing"
)

func TestCSlice(t *testing.T) {
	type args struct {
		items []CString
	}
	tests := []struct {
		name string
		args args
		want []Comparable
	}{
		{
			name: "Тестирование преобразования массива элементов определенного типа в Comparable",
			args: args{
				items: []CString{"test", "test1"},
			},
			want: []Comparable{
				CString("test"),
				CString("test1"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CSlice(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCSliceFalse(t *testing.T) {
	type args struct {
		items []string
	}
	tests := []struct {
		name string
		args args
		want []Comparable
	}{
		{
			name: "Тестирование преобразования массива простых элементов [Не Comparable] в Comparable",
			args: args{
				items: []string{"test", "test1"},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CSlice(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCSliceInterface(t *testing.T) {
	type args struct {
		items []Comparable
	}
	tests := []struct {
		name string
		args args
		want []Comparable
	}{
		{
			name: "Тестирование преобразования массива интерфейсов определенного типа в Comparable",
			args: args{
				items: []Comparable{
					CString("test"),
					CString("test1"),
				},
			},
			want: []Comparable{
				CString("test"),
				CString("test1"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CSlice(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
