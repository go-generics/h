package h

// ValueOrDefault возвращает либо переданное значение в первом аргументе,
// либо значение по умолчанию.
func ValueOrDefault[Val comparable](value Val, defaultValue Val) Val {
	var empty Val
	if empty != value {
		return value
	}

	return defaultValue
}
