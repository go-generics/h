package h

import "reflect"

// CSlice выполняет преобразование переданного массива элементов в тип
// Comparable, который можно использовать в других методах библиотеки.
//
// При вызове методов библиотеки не всегда удобно использовать именно массив
// интерфейсов Comparable, удобнее использовать реальный массив. Но, для этого
// необходимо зачастую преобразовать переданный массив вручную. Что позволяет
// избежать данная функция.
func CSlice[T any](items []T) []Comparable {
	if 0 == len(items) {
		return nil
	}

	resultType := reflect.TypeOf((*Comparable)(nil)).Elem()

	itemsType := reflect.TypeOf(items[0])
	if InArray(itemsType.Kind(), []reflect.Kind{
		reflect.Array,
		reflect.Chan,
		reflect.Map,
		reflect.Pointer,
		reflect.Slice,
	}) {
		itemsType = itemsType.Elem()
	}

	if !itemsType.Implements(resultType) {
		return nil
	}

	result := make([]Comparable, len(items))
	for i, item := range items {
		result[i] = (interface{})(item).(Comparable)
	}

	return result
}
