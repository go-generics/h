package h

import "time"

// WorSThree выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func WorSThree[Res any, Arg any, Arg2 any, Arg3 any](
	callback func(Arg, Arg2, Arg3) (Res, error),
) func(timeout time.Duration) func(arg Arg, arg2 Arg2, arg3 Arg3) (Res, error) {
	return func(timeout time.Duration) func(arg Arg, arg2 Arg2, arg3 Arg3) (Res, error) {
		return func(arg Arg, arg2 Arg2, arg3 Arg3) (Res, error) {
			return WaitOrSkip(timeout, func() (Res, error) {
				return callback(arg, arg2, arg3)
			})
		}
	}
}
