package h

import "time"

// EorSTwo выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func EorSTwo[Arg any, Arg2 any](
	callback func(Arg, Arg2) error,
) func(timeout time.Duration) func(Arg, Arg2) error {
	return func(timeout time.Duration) func(Arg, Arg2) error {
		return func(arg Arg, arg2 Arg2) error {
			_, err := WaitOrSkip(timeout, func() (any, error) {
				return nil, callback(arg, arg2)
			})

			return err
		}
	}
}
