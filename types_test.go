package h

// CompareStruct описывает тестовую структуру для проверки
// методов сравнения из библиотеки
type CompareStruct struct {
	Value string
}

// CompareKey реализует метод получения ключа для сравнения.
// Используется в методах библиотеки.
func (c CompareStruct) CKey() string {
	return c.Value
}

// CStringStruct реализует тестовый тип структуры для проверки
// наследования универсального строкового типа
type CStringStruct struct {
	CString
	Value int
}
