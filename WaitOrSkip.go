package h

import (
	"context"
	"time"
)

// WaitOrSkip реализует функционал ожидания выполнения блокируемой ф-ции с определенным
// периодом таймаута. Если блокирующая функция выполняется дольше переданного таймаута,
// то выполнение завершается ошибкой.
//
// Важно! WaitOrSkip не закрывает goroutine с реальной исполняемой функцией, т.к. в golang
// нет возможности прямого управления потоками. Необходимо самим убедиться, что функция
// завершится, иначе это приведет к утечке памяти!
//
// Важно так же учитывать накладные расходы на использование данной функции, т.к. она под
// капотом использует goroutine, что потребляет определенное количество ОЗУ, а так же требует
// дополнительного времени на инициализацию, что замедляет начало исполнения основной ф-ции.
func WaitOrSkip[Res any](timeout time.Duration, callback func() (Res, error)) (Res, error) {
	var noResult Res

	closed := SyncValue(false)
	defer closed.Set(true)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	result := SyncValue[Res](noResult)
	errorChan := make(chan error, 2)
	defer close(errorChan)

	go func() {
		res, err := callback()

		if !closed.Get() {
			errorChan <- err
			result.Set(res)
		}
	}()

	var err error

	select {
	case <-ctx.Done():
		err = ctx.Err()
	case e := <-errorChan:
		err = e
	}

	if err == context.DeadlineExceeded {
		err = WaitTimeoutError(`execution time is over`)
	}

	return result.Get(), err
}
