package h

// Filter выполняет фильтрацию переданных элеметов в массиве при помощи
// специальной функции сравнения, передаваемой вторым аргументом.
//
// Если функция сравнения возвращает false, то элемент исключается из
// результатов выдачи.
func Filter[T any](items []T, compareFn func(item T) bool) []T {
	if 0 == len(items) {
		return nil
	}

	result := make([]T, 0, len(items))
	for _, item := range items {
		if compareFn(item) {
			result = append(result, item)
		}
	}

	return result
}
