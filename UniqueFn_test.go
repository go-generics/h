package h

import (
	"reflect"
	"testing"
)

func TestUniqueFn(t *testing.T) {
	type args struct {
		items     []string
		compareFn func(a, b string) bool
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование получения уникальных строк",
			args: args{
				items: []string{`12`, `12`, `2`, `5`, `5`, `5`, `777`},
				compareFn: func(a, b string) bool {
					return a == b
				},
			},
			want: []string{`12`, `2`, `5`, `777`},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UniqueFn(tt.args.items, tt.args.compareFn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UniqueFn() = %v, want %v", got, tt.want)
			}
		})
	}
}
