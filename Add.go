package h

// Add выполняет добавление элемента в переданный список.
// Отличие от стандартной функции append в способе выделения памяти,
// в случае не хватки места в текущем массиве.
//
// В случае не хватки места функция выделяет память ровно на то количество
// элементов, которое передано через capacityStep (по умолчанию 10)
//
// Результатом работы функции будет массив, содержащий в себе элементы
// переданного, а так же добавленный в конец новый элемент.
func Add[T any](item T, items []T, capacityStep ...int) []T {
	if 0 == len(capacityStep) {
		capacityStep = append(capacityStep, 10)
	}

	capacityStepValue := capacityStep[0]
	if capacityStepValue < 1 {
		capacityStepValue = 10
	}

	if len(items) == cap(items) {
		newItems := make([]T, 0, cap(items)+capacityStepValue)
		for _, item := range items {
			newItems = append(newItems, item)
		}

		items = newItems
	}

	return append(items, item)
}
