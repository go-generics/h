package h

// Diff вычисляет расхождение массивов.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// отсутствуют во всех остальных переданных массивах.
func Diff[T comparable](array []T, arraysToDiff ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isNotFound := true

		for _, arrayToDiff := range arraysToDiff {
			if InArray(item, arrayToDiff) {
				isNotFound = false

				break
			}
		}

		if isNotFound {
			result = append(result, item)
		}
	}

	return result
}
