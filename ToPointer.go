package h

// ToPointer реализует функцию преобразования значения в указатель
func ToPointer[T any](val T) *T {
	item := val

	return &item
}
