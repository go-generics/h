package h

import "time"

// WorSOne выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func WorSOne[Res any, Arg any](
	callback func(Arg) (Res, error),
) func(timeout time.Duration) func(arg Arg) (Res, error) {
	return func(timeout time.Duration) func(arg Arg) (Res, error) {
		return func(arg Arg) (Res, error) {
			return WaitOrSkip(timeout, func() (Res, error) {
				return callback(arg)
			})
		}
	}
}
