package h

// DiffCompare вычисляет расхождение массивов Comparable.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// отсутствуют во всех остальных переданных массивах.
func DiffCompare[T Comparable](array []T, arraysToDiff ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isNotFound := true

		for _, arrayToDiff := range arraysToDiff {
			if InArrayCompare(item, arrayToDiff) {
				isNotFound = false

				break
			}
		}

		if isNotFound {
			result = append(result, item)
		}
	}

	return result
}
