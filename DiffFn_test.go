package h

import (
	"reflect"
	"testing"
)

func TestDiffFn(t *testing.T) {
	type args struct {
		array        []string
		compareFn    func(a, b string) bool
		arraysToDiff [][]string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование поиска расхождений",
			args: args{
				array: []string{"1", "2", "3"},
				compareFn: func(a, b string) bool {
					return a == b
				},
				arraysToDiff: [][]string{
					{"1", "3"},
					{"3", "4"},
				},
			},
			want: []string{"2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DiffFn(tt.args.compareFn, tt.args.array, tt.args.arraysToDiff...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DiffFn() = %v, want %v", got, tt.want)
			}
		})
	}
}
