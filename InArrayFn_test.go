package h

import "testing"

func TestInArrayFn(t *testing.T) {
	type args struct {
		item      int
		array     []int
		compareFn func(a, b int) bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование поиска элементов в переданном массиве",
			args: args{
				item:  100,
				array: []int{10, 50, 100},
				compareFn: func(a, b int) bool {
					return a == b
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InArrayFn(tt.args.item, tt.args.array, tt.args.compareFn); got != tt.want {
				t.Errorf("InArrayFn() = %v, want %v", got, tt.want)
			}
		})
	}
}
