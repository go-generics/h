package h

import (
	"reflect"
	"testing"
)

func TestFind(t *testing.T) {
	result := CString("test")
	type args struct {
		items     []CString
		compareFn func(item CString) bool
	}
	tests := []struct {
		name string
		args args
		want *CString
	}{
		{
			name: "Тестирование поиска строки CString",
			args: args{
				items: []CString{"test", "test1", "test2"},
				compareFn: func(item CString) bool {
					return item == "test"
				},
			},
			want: &result,
		},
		{
			name: "Тестирование поиска строки CString",
			args: args{
				items: []CString{"test", "test1", "test2"},
				compareFn: func(item CString) bool {
					return item == "test22"
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Find(tt.args.items, tt.args.compareFn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() = %v, want %v", got, tt.want)
			}
		})
	}
}
