package h

// UniqueFn возвращает уникальные элементы из переданного массива.
// В результат возвращаются все элементы в единичном экземпляре.
// Функция работает с любыми типами. Для сравнения эквивалентности
// используется функция `compareFn`
func UniqueFn[T any](items []T, compareFn func(a, b T) bool) []T {
	result := make([]T, 0, len(items))
	for _, item := range items {
		if !InArrayFn(item, result, compareFn) {
			result = append(result, item)
		}
	}

	return result
}
