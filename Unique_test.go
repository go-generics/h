package h

import (
	"reflect"
	"testing"
)

func TestUnique(t *testing.T) {
	type args struct {
		items []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование получения уникальных строк",
			args: args{
				items: []string{`12`, `12`, `2`, `5`, `5`, `5`, `777`},
			},
			want: []string{`12`, `2`, `5`, `777`},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Unique(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Unique() = %v, want %v", got, tt.want)
			}
		})
	}
}
