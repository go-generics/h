package h

// MergeMaps выполняет слияние нескольких однотипных map в одну
//
// В ходе работы возможны следующие нюансы:
//  - Если в нескольких map есть один и тот же ключ, то значение
//    в результатах будет соответствовать последней, переданной map
func MergeMaps[T comparable, K any](maps ...map[T]K) map[T]K {
	result := map[T]K{}
	for _, item := range maps {
		for key, value := range item {
			result[key] = value
		}
	}

	return result
}
