package h

import "reflect"

// IsEmpty реализует функцию, проверяющую значение, переданное через
// входящий аргумент и возвращающее статус - пустое оно или нет.
//
// Для скалярных типов проверка простая и сравнение происходит с начальным
// значением, например 0 для int.
// Для сложных типов:
// 1. Массивы, Каналы, Map - проверяется количество элементов. Если 0, то значение пустое.
// 2. Структуры - проверяется изначальное значение. Если структура соответствует ему - она пуста.
// 3. Указатели - если значение указателя = nil, структура пуста. Если нет, проверяется по значению указателя.
func IsEmpty[T any](value T) bool {
	rfc := reflect.ValueOf(value)
	rfcType := rfc.Kind()

	if rfcType == reflect.Ptr {
		if rfc.IsNil() {
			return true
		}

		rfc = rfc.Elem()
		rfcType = rfc.Kind()
	}

	ifcValue := ToInterface(value)
	switch v := ifcValue.(type) {
	case *int:
		return IsEmpty(*v)
	case *int8:
		return IsEmpty(*v)
	case *int16:
		return IsEmpty(*v)
	case *int32:
		return IsEmpty(*v)
	case *int64:
		return IsEmpty(*v)
	case *uint:
		return IsEmpty(*v)
	case *uint8:
		return IsEmpty(*v)
	case *uint16:
		return IsEmpty(*v)
	case *uint32:
		return IsEmpty(*v)
	case *uint64:
		return IsEmpty(*v)
	case *float32:
		return IsEmpty(*v)
	case *float64:
		return IsEmpty(*v)
	case *string:
		return IsEmpty(*v)
	case *complex64:
		return IsEmpty(*v)
	case *complex128:
		return IsEmpty(*v)
	case *uintptr:
		return IsEmpty(*v)
	case *bool:
		return false
	case int:
		return v == 0
	case int8:
		return v == 0
	case int16:
		return v == 0
	case int32:
		return v == 0
	case int64:
		return v == 0
	case uint:
		return v == 0
	case uint8:
		return v == 0
	case uint16:
		return v == 0
	case uint32:
		return v == 0
	case uint64:
		return v == 0
	case float32:
		return v == 0
	case float64:
		return v == 0
	case string:
		return v == ``
	case complex64:
		return v == 0
	case complex128:
		return v == 0
	case uintptr:
		return v == 0
	case bool:
		return false
	}

	if InArray(rfcType, []reflect.Kind{reflect.Array, reflect.Slice, reflect.Chan, reflect.Map}) {
		return rfc.Len() == 0
	}

	if rfcType == reflect.Struct {
		return rfc.IsZero()
	}

	var emptyVal T

	return reflect.DeepEqual(emptyVal, value)
}
