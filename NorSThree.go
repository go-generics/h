package h

import "time"

// NorSThree выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func NorSThree[Arg any, Arg2 any, Arg3 any](
	callback func(Arg, Arg2, Arg3),
) func(timeout time.Duration) func(Arg, Arg2, Arg3) {
	return func(timeout time.Duration) func(Arg, Arg2, Arg3) {
		return func(arg Arg, arg2 Arg2, arg3 Arg3) {
			_, _ = WaitOrSkip(timeout, func() (any, error) {
				callback(arg, arg2, arg3)

				return nil, nil
			})
		}
	}
}
