package h

import (
	"context"
	"github.com/pkg/errors"
)

// OneOf реализует функционал преобразования callback функции для получения
// сущностей по множеству ключей в callback получения сущности по одиночному
// ключу.
//
// Если сущность не найдена, возвращается `nil`.
func OneOf[Entity any, Tx any](
	multiple func(ctx context.Context, keys []string, tx Tx) ([]Entity, error),
) func(ctx context.Context, key string, tx Tx) (*Entity, error) {
	return func(ctx context.Context, key string, tx Tx) (*Entity, error) {
		entities, err := multiple(ctx, []string{key}, tx)
		if nil != err {
			return nil, errors.Wrap(err, `[OneOf]`)
		}

		if 0 == len(entities) {
			return nil, nil
		}

		entity := entities[0]

		return &entity, nil
	}
}
