package h

import "reflect"

// IsNil сравнивает переданное значение любого типа с nil и возвращает
// результат.
func IsNil[T any](value T) bool {
	ifcValue := ToInterface(value)
	switch ifcValue.(type) {
	case int:
		return false
	case int8:
		return false
	case int16:
		return false
	case int32:
		return false
	case int64:
		return false
	case uint:
		return false
	case uint8:
		return false
	case uint16:
		return false
	case uint32:
		return false
	case uint64:
		return false
	case float32:
		return false
	case float64:
		return false
	case string:
		return false
	case complex64:
		return false
	case complex128:
		return false
	case uintptr:
		return false
	case bool:
		return false
	}

	rfc := reflect.ValueOf(value)

	if rfc.Kind() == reflect.Ptr {
		return rfc.IsNil()
	}

	return ifcValue == nil
}
