package h

// Map выполняет преобразование переданного массива данных в другой формат,
// определяемый результатом работы функции - маппера, передаваемой вторым
// параметром.
func Map[T any, M any](items []T, mapFunc func(item T) M) []M {
	if 0 == len(items) {
		return nil
	}

	result := make([]M, 0, len(items))
	for _, item := range items {
		result = append(result, mapFunc(item))
	}

	return result
}
