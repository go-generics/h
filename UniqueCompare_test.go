package h

import (
	"reflect"
	"testing"
)

func TestUniqueCompare(t *testing.T) {
	type args struct {
		items []CompareStruct
	}
	tests := []struct {
		name string
		args args
		want []CompareStruct
	}{
		{
			name: "Тестирование получения уникальных структур",
			args: args{
				items: []CompareStruct{
					{Value: `12`},
					{Value: `5`},
					{Value: `12`},
					{Value: `4`},
					{Value: `5`},
					{Value: `777`},
					{Value: `12`},
				},
			},
			want: []CompareStruct{
				{Value: `12`},
				{Value: `5`},
				{Value: `4`},
				{Value: `777`},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UniqueCompare(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UniqueCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
