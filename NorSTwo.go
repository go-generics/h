package h

import "time"

// NorSTwo выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func NorSTwo[Arg any, Arg2 any](
	callback func(Arg, Arg2),
) func(timeout time.Duration) func(Arg, Arg2) {
	return func(timeout time.Duration) func(Arg, Arg2) {
		return func(arg Arg, arg2 Arg2) {
			_, _ = WaitOrSkip(timeout, func() (any, error) {
				callback(arg, arg2)

				return nil, nil
			})
		}
	}
}
