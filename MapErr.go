package h

import (
	"github.com/pkg/errors"
	"strconv"
)

// MapErr выполняет преобразование переданного массива данных в другой формат,
// определяемый результатом работы функции - маппера, передаваемой вторым
// параметром.
//
// Если функция - маппер возвращает ошибку, то результатом работы функции будет
// ошибка, с указанием элемента, на котором она произошла.
func MapErr[T any, M any](items []T, mapFunc func(item T) (M, error)) ([]M, error) {
	if 0 == len(items) {
		return nil, nil
	}

	result := make([]M, 0, len(items))
	for i, item := range items {
		mapped, err := mapFunc(item)
		if nil != err {
			return nil, errors.Wrap(err, `failed to map item #`+strconv.Itoa(i))
		}

		result = append(result, mapped)
	}

	return result, nil
}
