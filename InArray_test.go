package h

import "testing"

func TestInArray(t *testing.T) {
	type args struct {
		item  int
		array []int
	}

	type test struct {
		name string
		args args
		want bool
	}

	tests := []test{
		{
			name: "Тестирование наличия элемента в массиве",
			args: args{
				item:  8,
				array: []int{1, 3, 6, 8},
			},
			want: true,
		},
		{
			name: "Тестирование наличия элемента в массиве",
			args: args{
				item:  24,
				array: []int{1, 3, 6, 8},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InArray(tt.args.item, tt.args.array); got != tt.want {
				t.Errorf("InArray() = %v, want %v", got, tt.want)
			}
		})
	}
}
