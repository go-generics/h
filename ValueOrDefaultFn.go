package h

// ValueOrDefaultFn возвращает либо переданное значение в первом аргументе,
// либо значение по умолчанию. Для определения наличия значения используется
// функция isEmpty
func ValueOrDefaultFn[Val any](value Val, defaultValue Val, isEmpty func(Val) bool) Val {
	if !isEmpty(value) {
		return value
	}

	return defaultValue
}
