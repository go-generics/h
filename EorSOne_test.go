package h

import (
	"github.com/pkg/errors"
	"testing"
	"time"
)

func TestEorSOne(t *testing.T) {
	type args struct {
		callback func(string) error
		value    string
		timeOut  time.Duration
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Тестирование работы функции с корректным результатом",
			args: args{
				callback: func(s string) error {
					return nil
				},
				value:   "test",
				timeOut: 10 * time.Second,
			},
			wantErr: false,
		},
		{
			name: "Тестирование работы функции с не корректным результатом",
			args: args{
				callback: func(s string) error {
					return errors.New(s)
				},
				value:   "test",
				timeOut: 10 * time.Second,
			},
			wantErr: true,
		},
		{
			name: "Тестирование работы функции с задержкой выполнения",
			args: args{
				callback: func(s string) error {
					time.Sleep(10 * time.Second)

					return errors.New(s)
				},
				value:   "test",
				timeOut: 100 * time.Millisecond,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := EorSOne(tt.args.callback)(tt.args.timeOut)(tt.args.value)

			if (err != nil) != tt.wantErr {
				t.Errorf("WaitOrSkip() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
