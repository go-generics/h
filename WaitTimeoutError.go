package h

// WaitTimeoutError описывает специальный тип ошибки, который
// позволяет определить отвал выполнения функций по таймауту.
type WaitTimeoutError string

// Error возвращает текст ошибки
func (w WaitTimeoutError) Error() string {
	return string(w)
}
