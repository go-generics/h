package h

import (
	"reflect"
	"testing"
)

func TestDiff(t *testing.T) {
	type args struct {
		array        []string
		arraysToDiff [][]string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование поиска расхождений",
			args: args{
				array: []string{"1", "2", "3"},
				arraysToDiff: [][]string{
					{"1", "3"},
					{"3", "4"},
				},
			},
			want: []string{"2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Diff(tt.args.array, tt.args.arraysToDiff...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Diff() = %v, want %v", got, tt.want)
			}
		})
	}
}
