package h

import (
	"reflect"
	"testing"
)

func TestDiffCompare(t *testing.T) {
	type args struct {
		array        []CompareStruct
		arraysToDiff [][]CompareStruct
	}
	tests := []struct {
		name string
		args args
		want []CompareStruct
	}{
		{
			name: "Тестирование получения расхождения",
			args: args{
				array: []CompareStruct{
					{Value: "1"},
					{Value: "2"},
					{Value: "3"},
				},
				arraysToDiff: [][]CompareStruct{
					{
						{Value: "1"},
						{Value: "3"},
					},
					{
						{Value: "4"},
						{Value: "3"},
					},
				},
			},
			want: []CompareStruct{
				{Value: "2"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DiffCompare(tt.args.array, tt.args.arraysToDiff...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DiffCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
