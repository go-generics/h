package h

import (
	"reflect"
	"testing"
)

func TestFilter(t *testing.T) {
	type args struct {
		items     []int
		compareFn func(item int) bool
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Тестирование фильтрации значений больше 5",
			args: args{
				items: []int{0, 1, 3, 5, 7, 4, 9},
				compareFn: func(item int) bool {
					return item > 5
				},
			},
			want: []int{7, 9},
		},
		{
			name: "Тестирование фильтрации значений меньше 5",
			args: args{
				items: []int{0, 1, 3, 5, 7, 4, 9},
				compareFn: func(item int) bool {
					return item < 5
				},
			},
			want: []int{0, 1, 3, 4},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Filter(tt.args.items, tt.args.compareFn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}
