package h

// ValueOrDefaultCompare возвращает либо переданное значение в первом аргументе,
// либо значение по умолчанию. Для сравнения используются сущности, реализующие
// интерфейс Comparable
func ValueOrDefaultCompare[Val Comparable](value Val, defaultValue Val) Val {
	var empty Val
	if empty.CKey() != value.CKey() {
		return value
	}

	return defaultValue
}
