package h

import "testing"

func TestInArrayCompare(t *testing.T) {
	type args struct {
		item  Comparable
		array []Comparable
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование проверки наличия элементов в массиве",
			args: args{
				item: CompareStruct{
					Value: "test",
				},
				array: []Comparable{
					CompareStruct{
						Value: "test1",
					},
					CompareStruct{
						Value: "test2",
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование проверки наличия элементов в массиве",
			args: args{
				item: CompareStruct{
					Value: "test2",
				},
				array: []Comparable{
					CompareStruct{
						Value: "test1",
					},
					CompareStruct{
						Value: "test2",
					},
				},
			},
			want: true,
		},
		{
			name: "Тестирование проверки наличия элементов в массиве",
			args: args{
				item: CStringStruct{
					CString: "test",
					Value:   123,
				},
				array: []Comparable{
					CStringStruct{
						CString: "test2",
						Value:   1234,
					},
					CStringStruct{
						CString: "test34",
						Value:   1223,
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование проверки наличия элементов в массиве",
			args: args{
				item: CStringStruct{
					CString: "test34",
					Value:   123,
				},
				array: []Comparable{
					CStringStruct{
						CString: "test2",
						Value:   1234,
					},
					CStringStruct{
						CString: "test34",
						Value:   1223,
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InArrayCompare[Comparable](tt.args.item, tt.args.array); got != tt.want {
				t.Errorf("InArrayCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInArrayCompareCStringStruct(t *testing.T) {
	type args struct {
		item  CStringStruct
		array []CStringStruct
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование проверки наличия элементов в массиве",
			args: args{
				item: CStringStruct{
					CString: "test34",
					Value:   123,
				},
				array: []CStringStruct{
					{
						CString: "test2",
						Value:   1234,
					},
					{
						CString: "test34",
						Value:   1223,
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InArrayCompare[CStringStruct](tt.args.item, tt.args.array); got != tt.want {
				t.Errorf("InArrayCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
