package h

import (
	"reflect"
	"testing"
)

func TestAdd(t *testing.T) {
	type args struct {
		item         string
		items        []string
		capacityStep []int
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantCap int
	}{
		{
			name: "Тестирование добавления элемента",
			args: args{
				item:         "test",
				items:        make([]string, 0, 5),
				capacityStep: nil,
			},
			want:    []string{"test"},
			wantCap: 5,
		},
		{
			name: "Тестирование добавления элемента",
			args: args{
				item:         "test",
				items:        make([]string, 0, 1),
				capacityStep: nil,
			},
			want:    []string{"test"},
			wantCap: 1,
		},
		{
			name: "Тестирование добавления элемента",
			args: args{
				item:         "test",
				items:        []string{},
				capacityStep: nil,
			},
			want:    []string{"test"},
			wantCap: 10,
		},
		{
			name: "Тестирование добавления элемента",
			args: args{
				item:         "test",
				items:        []string{"test1"},
				capacityStep: []int{5},
			},
			want:    []string{"test1", "test"},
			wantCap: 6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Add(tt.args.item, tt.args.items, tt.args.capacityStep...)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Add() = %v, want %v", got, tt.want)
			}

			if cap(got) != tt.wantCap {
				t.Errorf("Add() cap = %v, want %v", cap(got), tt.wantCap)
			}
		})
	}
}
