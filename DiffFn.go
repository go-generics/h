package h

// DiffFn вычисляет расхождение массивов.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// отсутствуют во всех остальных переданных массивах.
//
// Сравнение элементов происходит при помощи специальной функции сравнения.
func DiffFn[T any](compareFn func(a, b T) bool, array []T, arraysToDiff ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isNotFound := true

		for _, arrayToDiff := range arraysToDiff {
			if InArrayFn(item, arrayToDiff, compareFn) {
				isNotFound = false

				break
			}
		}

		if isNotFound {
			result = append(result, item)
		}
	}

	return result
}
