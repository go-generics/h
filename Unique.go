package h

// Unique возвращает уникальные элементы из переданного массива.
// В результат возвращаются все элементы в единичном экземпляре.
func Unique[T comparable](items []T) []T {
	result := make([]T, 0, len(items))
	for _, item := range items {
		if !InArray(item, result) {
			result = append(result, item)
		}
	}

	return result
}
