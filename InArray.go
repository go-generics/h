package h

// InArray проверяет наличие переданного элемента в переданном массиве.
// Для использования допускаются только сравниваемые элементы.
//
// В случае, если элемент есть в переданном массиве, возвращается TRUE,
// иначе FALSE.
func InArray[T comparable](item T, array []T) bool {
	for _, exists := range array {
		if item == exists {
			return true
		}
	}

	return false
}
