package h

import (
	"reflect"
	"testing"
)

func TestFlat(t *testing.T) {
	type args struct {
		items [][][]int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Тестирование разворачивания массивов",
			args: args{
				items: [][][]int{
					{{1, 2}, {3, 4}},
					{{5}},
				},
			},
			want: []int{1, 2, 3, 4, 5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Flat(tt.args.items...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Flat() = %v, want %v", got, tt.want)
			}
		})
	}
}
