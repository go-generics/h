package h

// InArrayFn проверяет наличие переданного элемента в переданном массиве,
// при помощи специальной функции сравнения элементов.
//
// В случае, если элемент есть в переданном массиве, возвращается TRUE,
// иначе FALSE.
func InArrayFn[T any](item T, array []T, compareFn func(a, b T) bool) bool {
	for _, exists := range array {
		if compareFn(item, exists) {
			return true
		}
	}

	return false
}
