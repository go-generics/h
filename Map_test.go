package h

import (
	"reflect"
	"strconv"
	"testing"
)

func TestMap(t *testing.T) {
	type args struct {
		items   []int64
		mapFunc func(item int64) string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование преобразования чисел в строки",
			args: args{
				items: []int64{1, 2, 3, 100500},
				mapFunc: func(item int64) string {
					return strconv.FormatInt(item, 10)
				},
			},
			want: []string{`1`, `2`, `3`, `100500`},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Map(tt.args.items, tt.args.mapFunc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Map() = %v, want %v", got, tt.want)
			}
		})
	}
}
