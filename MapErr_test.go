package h

import (
	"github.com/pkg/errors"
	"reflect"
	"strconv"
	"testing"
)

func TestMapErr(t *testing.T) {
	type args struct {
		items   []int
		mapFunc func(item int) (string, error)
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Тестирование портирования int -> string. В процессе возникла ошибка.",
			args: args{
				items: []int{1, 2, 3},
				mapFunc: func(item int) (string, error) {
					return ``, errors.New(`test`)
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование портирования int -> string. Ошибок нет.",
			args: args{
				items: []int{1, 2, 3},
				mapFunc: func(item int) (string, error) {
					return strconv.Itoa(item), nil
				},
			},
			want:    []string{`1`, `2`, `3`},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := MapErr(tt.args.items, tt.args.mapFunc)
			if (err != nil) != tt.wantErr {
				t.Errorf("MapErr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MapErr() got = %v, want %v", got, tt.want)
			}
		})
	}
}
