package h

import "time"

// NorSOne выполняет генерацию функции, которая будет останавливаться по таймауту
// из переданного callback.
//
// Описание функционала можно посмотреть в WaitOrSkip
func NorSOne[Arg any](
	callback func(Arg),
) func(timeout time.Duration) func(Arg) {
	return func(timeout time.Duration) func(Arg) {
		return func(arg Arg) {
			_, _ = WaitOrSkip(timeout, func() (any, error) {
				callback(arg)

				return nil, nil
			})
		}
	}
}
