package h

// Flat разворачивает массивы, переданные в качестве аргументов
// и объединяет их элементы.
//
// По сути выполняется следующая логика: достаются элементы каждого массива
// и складываются в один результирующий массив.
//
// Передавая к примеру массивы [[1, 2], [3, 4]] и [[5]] будет сформирован
// массив - [1, 2, 3, 4, 5].
func Flat[T any](items ...[][]T) []T {
	if 0 == len(items) {
		return nil
	}

	length := 1
	for _, arrays := range items {
		for _, array := range arrays {
			length += len(array)
		}
	}

	result := make([]T, 0, length)
	for _, arrays := range items {
		for _, array := range arrays {
			result = append(result, array...)
		}
	}

	return result
}
