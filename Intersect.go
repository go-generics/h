package h

// Intersect вычисляет пересечение массивов.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// найдены во всех остальных переданных массивах.
func Intersect[T comparable](array []T, arraysToIntersect ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isFound := true

		for _, arrayToDiff := range arraysToIntersect {
			if !InArray(item, arrayToDiff) {
				isFound = false

				break
			}
		}

		if isFound {
			result = append(result, item)
		}
	}

	return result
}
