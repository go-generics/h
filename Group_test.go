package h

import (
	"reflect"
	"testing"
)

func TestGroup(t *testing.T) {
	type TestStruct struct {
		Type string
		Name string
	}

	type args struct {
		groupCompare func(a, b TestStruct) bool
		items        [][]TestStruct
	}
	tests := []struct {
		name string
		args args
		want [][]TestStruct
	}{
		{
			name: "Тестирование группировки элементов",
			args: args{
				groupCompare: func(a, b TestStruct) bool {
					return a.Type == b.Type
				},
				items: [][]TestStruct{
					{
						{
							Type: "one",
							Name: "test1",
						},
						{
							Type: "two",
							Name: "test2",
						},
						{
							Type: "one",
							Name: "test3",
						},
						{
							Type: "three",
							Name: "test4",
						},
						{
							Type: "two",
							Name: "test5",
						},
						{
							Type: "one",
							Name: "test6",
						},
					},
				},
			},
			want: [][]TestStruct{
				{
					{
						Type: "one",
						Name: "test1",
					},
					{
						Type: "one",
						Name: "test3",
					},
					{
						Type: "one",
						Name: "test6",
					},
				},
				{
					{
						Type: "two",
						Name: "test2",
					},
					{
						Type: "two",
						Name: "test5",
					},
				},
				{
					{
						Type: "three",
						Name: "test4",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Group(tt.args.groupCompare, tt.args.items...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Group() = %v, want %v", got, tt.want)
			}
		})
	}
}
