package h

// IntersectFn вычисляет пересечение массивов.
//
// Возвращает массив, состоящий из элементов первого массива, которые
// найдены во всех остальных переданных массивах.
//
// Сравнение элементов происходит при помощи специальной функции сравнения.
func IntersectFn[T any](compareFn func(a, b T) bool, array []T, arraysToIntersect ...[]T) []T {
	if 0 == len(array) {
		return nil
	}

	result := make([]T, 0, len(array))
	for _, item := range array {
		isFound := true

		for _, arrayToDiff := range arraysToIntersect {
			if !InArrayFn(item, arrayToDiff, compareFn) {
				isFound = false

				break
			}
		}

		if isFound {
			result = append(result, item)
		}
	}

	return result
}
