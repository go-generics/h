package h

import (
	"reflect"
	"testing"
)

func TestIntersect(t *testing.T) {
	type args struct {
		array             []string
		arraysToIntersect [][]string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Тестирование получения пересечения массивов",
			args: args{
				array: []string{"1", "2", "3"},
				arraysToIntersect: [][]string{
					{"2", "3", "4"},
					{"2", "1", "22"},
				},
			},
			want: []string{"2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Intersect(tt.args.array, tt.args.arraysToIntersect...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Intersect() = %v, want %v", got, tt.want)
			}
		})
	}
}
