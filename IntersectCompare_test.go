package h

import (
	"reflect"
	"testing"
)

func TestIntersectCompare(t *testing.T) {
	type args struct {
		array             []CompareStruct
		arraysToIntersect [][]CompareStruct
	}
	tests := []struct {
		name string
		args args
		want []CompareStruct
	}{
		{
			name: "Тестирование получения пересечения",
			args: args{
				array: []CompareStruct{
					{Value: "1"},
					{Value: "2"},
					{Value: "3"},
				},
				arraysToIntersect: [][]CompareStruct{
					{
						{Value: "1"},
						{Value: "3"},
					},
					{
						{Value: "4"},
						{Value: "3"},
					},
				},
			},
			want: []CompareStruct{
				{Value: "3"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IntersectCompare(tt.args.array, tt.args.arraysToIntersect...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("IntersectCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
