package h

import (
	"errors"
	"reflect"
	"strconv"
	"testing"
	"time"
)

func TestWaitOrSkip(t *testing.T) {
	type args struct {
		timeout  int
		callback func() (string, error)
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование отработки функции без ошибок",
			args: args{
				timeout: 1000,
				callback: func() (string, error) {
					return "1", nil
				},
			},
			want:    "1",
			wantErr: false,
		},
		{
			name: "Тестирование отработки функции с ошибкой исполнения",
			args: args{
				timeout: 1000,
				callback: func() (string, error) {
					return "", errors.New(`test`)
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование отработки функции с зависанием",
			args: args{
				timeout: 100,
				callback: func() (string, error) {
					time.Sleep(1 * time.Second)

					return "1", nil
				},
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := WaitOrSkip(time.Duration(tt.args.timeout)*time.Millisecond, tt.args.callback)
			if (err != nil) != tt.wantErr {
				t.Errorf("WaitOrSkip() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WaitOrSkip() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWaitOrSkip_loop(t *testing.T) {
	for j := 0; j < 3; j++ {
		t.Run("Iteration (1000000 ops) #"+strconv.Itoa(j), func(t *testing.T) {
			for i := 0; i < 1000000; i++ {
				_, err := WaitOrSkip(10*time.Second, func() (string, error) {
					return "", nil
				})

				if nil != err {
					t.Errorf("WaitOrSkip() error = %v, wantErr %v", err, false)
				}
			}
		})
	}
}

func TestWaitOrSkip_loopErr(t *testing.T) {
	for j := 0; j < 3; j++ {
		t.Run("Iteration (1000000 ops) #"+strconv.Itoa(j), func(t *testing.T) {
			for i := 0; i < 1000000; i++ {
				_, err := WaitOrSkip(10*time.Second, func() (string, error) {
					return "", errors.New(`test`)
				})

				if nil == err {
					t.Errorf("WaitOrSkip() error = %v, wantErr %v", err, true)
				}
			}
		})
	}
}

func TestWaitOrSkip_loopWait(t *testing.T) {
	for j := 0; j < 3; j++ {
		t.Run("Iteration (1500 waits) #"+strconv.Itoa(j), func(t *testing.T) {
			for i := 0; i < 1500; i++ {
				_, err := WaitOrSkip(1*time.Millisecond, func() (string, error) {
					time.Sleep(200 * time.Millisecond)

					return "", nil
				})

				if nil == err {
					t.Errorf("WaitOrSkip() error = %v, wantErr %v", err, true)
				}
			}
		})
	}
}
