package h

import (
	"reflect"
	"testing"
)

func TestMergeMaps(t *testing.T) {
	type args struct {
		maps []map[string]int
	}
	tests := []struct {
		name string
		args args
		want map[string]int
	}{
		{
			name: "Тестирование слияния карт",
			args: args{
				maps: []map[string]int{
					{`test1`: 123},
					{`test2`: 234},
					{`test3`: 345},
				},
			},
			want: map[string]int{
				`test1`: 123,
				`test2`: 234,
				`test3`: 345,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeMaps(tt.args.maps...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeMaps() = %v, want %v", got, tt.want)
			}
		})
	}
}
